from setuptools import setup

setup(
    name='pomodoro',
    version='0.0.0',
    packages=['pomodoro'],
    entry_points={
        'console_scripts': ['pomodoro=pomodoro.main:main'],
    }
)
