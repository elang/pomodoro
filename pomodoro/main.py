import sys
import time

def beep(freq, dur_ms):
    import platform
    if platform.system() == "Windows":
        import winsound
        winsound.Beep(freq, dur_ms)
    else:
        import subprocess
        subprocess.call("play --no-show-progress --null --channels 1 synth {} sine {}".format(dur_ms / 1000.0, freq), shell=True)

def fmt_time(secs):
    mins = int(secs / 60)
    secs = int(secs - mins * 60)
    return "{0}:{1:02d} ".format(mins, secs)

def count_down(t0, mins):
    while True:
        remaining = mins * 60 - (time.time() - t0)
        print(fmt_time(remaining), end="\r")
        if remaining <= 0:
            break
        time.sleep(min(0.5, remaining))

def count_up(t0):
    while True:
        elapsed = time.time() - t0
        print('-' + fmt_time(elapsed), end="\r")
        time.sleep(0.5)

def main():
    if len(sys.argv) != 2:
        print("Usage: <time in mins>")
    else:
        try:
            t0 = time.time()
            mins = float(sys.argv[1])
            count_down(t0, mins)
            beep(freq=400, dur_ms=100)
            count_up(t0 + mins * 60)
        except KeyboardInterrupt:
            pass
